set(TARGET watchdog_concept)

add_library(${TARGET} INTERFACE)
add_library(watchdog::concept ALIAS ${TARGET})

target_include_directories(${TARGET} INTERFACE include)

target_sources(${TARGET} PRIVATE
    include/watchdog/concepts/watchdog.h
    )

target_link_libraries(${TARGET} INTERFACE
    satos::api
    )
