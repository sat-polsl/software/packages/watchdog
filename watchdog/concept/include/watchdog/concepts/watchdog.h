#pragma once
#include "satos/chrono.h"

namespace watchdog::concepts {

// clang-format off
template<typename T>
concept watchdog = requires(T watchdog, satos::chrono::seconds seconds) {
    { watchdog.kick() };
    { watchdog.feed() };
    { watchdog.register_thread(seconds) };
    { watchdog.unregister_thread() };
};
// clang-format on

} // namespace watchdog::concepts
