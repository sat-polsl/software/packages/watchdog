#include "watchdog/thread_watchdog/thread_watchdog.h"
#include <ranges>
#include "satext/assert.h"

namespace watchdog {

thread_watchdog::thread_watchdog(std::span<thread_data> buffer,
                                 satext::inplace_function<void()> kick_callback) :
    threads_{buffer.data(), buffer.size()},
    kick_callback_{std::move(kick_callback)} {}

void thread_watchdog::register_thread(satos::clock::duration timeout,
                                      satos::thread_native_handle thread) {
    satext::expects(thread != nullptr);

    if (auto it = std::ranges::find(threads_, thread, &thread_data::thread); it != threads_.end()) {
        it->last_feed_time = satos::clock::now();
        it->timeout = timeout;
    } else {
        threads_.emplace_back(thread, satos::clock::now(), timeout);
    }
}

void thread_watchdog::unregister_thread(satos::thread_native_handle thread) {
    auto [begin, end] = std::ranges::remove(threads_, thread, &thread_data::thread);
    threads_.erase(begin, end);
}

void thread_watchdog::feed(satos::thread_native_handle thread) {
    if (auto it = std::ranges::find(threads_, thread, &thread_data::thread); it != threads_.end()) {
        it->last_feed_time = satos::clock::now();
    }
}

void thread_watchdog::kick() const {
    bool kick_watchdog{true};

    for (auto&& thread : threads_) {
        if (satos::clock::now() - thread.last_feed_time > thread.timeout) {
            kick_watchdog = false;
        }
    }

    if (kick_watchdog) {
        kick_callback_();
    }
}

} // namespace watchdog
