#pragma once
#include <span>
#include "etl/map.h"
#include "etl/vector.h"
#include "satext/inplace_function.h"
#include "satos/thread.h"
#include "watchdog/thread_watchdog/types.h"

namespace watchdog {

/**
 * @ingroup thread_watchdog
 * @{
 */

/**
 * @brief Thread watchdog.
 */
class thread_watchdog {
public:
    /**
     * @brief Constructor.
     * @param buffer Thread watchdog buffer.
     * @param kick_callback Kick callback.
     */
    thread_watchdog(std::span<thread_data> buffer, satext::inplace_function<void()> kick_callback);

    /**
     * @brief Register thread. If thread is already registered it will update timeout.
     * @param timeout Thread timeout.
     * @param thread Thread native handle.
     */
    void
    register_thread(satos::clock::duration timeout,
                    satos::thread_native_handle thread = satos::this_thread::get_native_handle());

    /**
     * @brief Unregister thread.
     * @param thread Thread native handle.
     */
    void
    unregister_thread(satos::thread_native_handle thread = satos::this_thread::get_native_handle());

    /**
     * @brief Feed watchdog.
     * @param thread Thread native handle.
     */
    void feed(satos::thread_native_handle thread = satos::this_thread::get_native_handle());

    /**
     * @brief Kick watchdog.
     */
    void kick() const;

private:
    etl::vector_ext<thread_data> threads_;
    satext::inplace_function<void()> kick_callback_;
};

/** @} */

} // namespace watchdog
