#pragma once
#include <array>
#include "satos/clock.h"
#include "satos/thread.h"

namespace watchdog {

/**
 * @ingroup thread_watchdog
 * @{
 */

/**
 * @brief Thread watchdog data.
 */
struct thread_data {
    /**
     * @brief Thread native handle.
     */
    satos::thread_native_handle thread{};

    /**
     * @brief Last watchdog feed time.
     */
    satos::clock::time_point last_feed_time{};

    /**
     * @brief Watchdog thread timeout.
     */
    satos::clock::duration timeout{};
};

/**
 * @brief Thread watchdog buffer.
 * @tparam Size Buffer size.
 */
template<std::size_t Size>
using watchdog_buffer = std::array<thread_data, Size>;

/** @} */

} // namespace watchdog
