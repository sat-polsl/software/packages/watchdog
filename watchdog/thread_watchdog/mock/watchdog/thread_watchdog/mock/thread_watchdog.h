#pragma once
#include "gmock/gmock.h"
#include "satos/thread.h"
#include "watchdog/concepts/watchdog.h"

namespace watchdog::mock {

struct thread_watchdog {
    MOCK_METHOD(void, register_thread_mock, (satos::clock::duration, satos::thread_native_handle));
    MOCK_METHOD(void, unregister_thread_mock, (satos::thread_native_handle));
    MOCK_METHOD(void, feed_mock, (satos::thread_native_handle));
    MOCK_METHOD(void, kick, ());

    void
    register_thread(satos::clock::duration timeout,
                    satos::thread_native_handle thread = satos::this_thread::get_native_handle()) {
        register_thread_mock(timeout, thread);
    }

    void unregister_thread(
        satos::thread_native_handle thread = satos::this_thread::get_native_handle()) {
        unregister_thread_mock(thread);
    }

    void feed(satos::thread_native_handle thread = satos::this_thread::get_native_handle()) {
        feed_mock(thread);
    }
};

static_assert(watchdog::concepts::watchdog<thread_watchdog>);

} // namespace watchdog::mock
