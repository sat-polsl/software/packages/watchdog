#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "satos/mock/clock.h"
#include "satos/mock/this_thread.h"
#include "watchdog/concepts/watchdog.h"
#include "watchdog/thread_watchdog/thread_watchdog.h"

namespace {
using namespace ::testing;
using namespace satos::chrono_literals;

static_assert(watchdog::concepts::watchdog<watchdog::thread_watchdog>);

struct WatchdogMock {
    MOCK_METHOD(void, kick, ());
};

struct ThreadWatchdogTest : public Test {
    watchdog::watchdog_buffer<5> buffer_{};
    StrictMock<WatchdogMock> watchdog_mock_{};
    satos::mock::this_thread& this_thread_{satos::mock::this_thread::instance()};
    satos::mock::clock& clock_{satos::mock::clock::instance()};
};

TEST_F(ThreadWatchdogTest, RegisterThread) {
    watchdog::thread_watchdog thread_watchdog{buffer_, []() {}};

    EXPECT_CALL(this_thread_, get_native_handle())
        .WillOnce(Return(satos::thread_native_handle(0xdeadc0de)));
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{10_s}));
    thread_watchdog.register_thread(1_s);

    EXPECT_THAT(buffer_[0].thread, Eq(satos::thread_native_handle(0xdeadc0de)));
    EXPECT_THAT(buffer_[0].last_feed_time, Eq(satos::clock::time_point{10_s}));
    EXPECT_THAT(buffer_[0].timeout, Eq(1_s));
}

TEST_F(ThreadWatchdogTest, RegisterSameThread) {
    watchdog::thread_watchdog thread_watchdog{buffer_, []() {}};

    InSequence s;
    EXPECT_CALL(this_thread_, get_native_handle())
        .WillOnce(Return(satos::thread_native_handle(0xdeadc0de)));
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{10_s}));
    thread_watchdog.register_thread(1_s);

    EXPECT_THAT(buffer_[0].thread, Eq(satos::thread_native_handle(0xdeadc0de)));
    EXPECT_THAT(buffer_[0].last_feed_time, Eq(satos::clock::time_point{10_s}));
    EXPECT_THAT(buffer_[0].timeout, Eq(1_s));

    EXPECT_CALL(this_thread_, get_native_handle())
        .WillOnce(Return(satos::thread_native_handle(0xdeadc0de)));
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{20_s}));
    thread_watchdog.register_thread(10_s);

    EXPECT_THAT(buffer_[0].thread, Eq(satos::thread_native_handle(0xdeadc0de)));
    EXPECT_THAT(buffer_[0].last_feed_time, Eq(satos::clock::time_point{20_s}));
    EXPECT_THAT(buffer_[0].timeout, Eq(10_s));

    EXPECT_THAT(buffer_[1].thread, Eq(satos::thread_native_handle(nullptr)));
}

TEST_F(ThreadWatchdogTest, RegisterMultipleThreads) {
    watchdog::thread_watchdog thread_watchdog{buffer_, []() {}};

    InSequence s;
    EXPECT_CALL(this_thread_, get_native_handle())
        .WillOnce(Return(satos::thread_native_handle(0xdeadc0de)));
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{10_s}));
    thread_watchdog.register_thread(1_s);

    EXPECT_THAT(buffer_[0].thread, Eq(satos::thread_native_handle(0xdeadc0de)));
    EXPECT_THAT(buffer_[0].last_feed_time, Eq(satos::clock::time_point{10_s}));
    EXPECT_THAT(buffer_[0].timeout, Eq(1_s));

    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{20_s}));
    thread_watchdog.register_thread(10_s, satos::thread_native_handle(0xdeadbeef));

    EXPECT_THAT(buffer_[1].thread, Eq(satos::thread_native_handle(0xdeadbeef)));
    EXPECT_THAT(buffer_[1].last_feed_time, Eq(satos::clock::time_point{20_s}));
    EXPECT_THAT(buffer_[1].timeout, Eq(10_s));
}

TEST_F(ThreadWatchdogTest, UnregisterThread) {
    watchdog::thread_watchdog thread_watchdog{buffer_, []() {}};

    InSequence s;
    EXPECT_CALL(this_thread_, get_native_handle())
        .WillOnce(Return(satos::thread_native_handle(0xdeadc0de)));
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{10_s}));
    thread_watchdog.register_thread(1_s);

    EXPECT_THAT(buffer_[0].thread, Eq(satos::thread_native_handle(0xdeadc0de)));
    EXPECT_THAT(buffer_[0].last_feed_time, Eq(satos::clock::time_point{10_s}));
    EXPECT_THAT(buffer_[0].timeout, Eq(1_s));

    thread_watchdog.unregister_thread(satos::thread_native_handle(0xdeadc0de));

    EXPECT_CALL(this_thread_, get_native_handle())
        .WillOnce(Return(satos::thread_native_handle(0xdeadbeef)));
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1_s}));
    thread_watchdog.register_thread(100_ms);

    EXPECT_THAT(buffer_[0].thread, Eq(satos::thread_native_handle(0xdeadbeef)));
    EXPECT_THAT(buffer_[0].last_feed_time, Eq(satos::clock::time_point{1_s}));
    EXPECT_THAT(buffer_[0].timeout, Eq(100_ms));
}

TEST_F(ThreadWatchdogTest, Feed) {
    watchdog::thread_watchdog thread_watchdog{buffer_, []() {}};

    InSequence s;
    EXPECT_CALL(this_thread_, get_native_handle())
        .WillOnce(Return(satos::thread_native_handle(0xdeadc0de)));
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{10_s}));
    thread_watchdog.register_thread(1_s);

    EXPECT_THAT(buffer_[0].thread, Eq(satos::thread_native_handle(0xdeadc0de)));
    EXPECT_THAT(buffer_[0].last_feed_time, Eq(satos::clock::time_point{10_s}));
    EXPECT_THAT(buffer_[0].timeout, Eq(1_s));

    EXPECT_CALL(this_thread_, get_native_handle())
        .WillOnce(Return(satos::thread_native_handle(0xdeadc0de)));
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{20_s}));
    thread_watchdog.feed();
    EXPECT_THAT(buffer_[0].thread, Eq(satos::thread_native_handle(0xdeadc0de)));
    EXPECT_THAT(buffer_[0].last_feed_time, Eq(satos::clock::time_point{20_s}));
    EXPECT_THAT(buffer_[0].timeout, Eq(1_s));
}

TEST_F(ThreadWatchdogTest, Kick) {
    watchdog::thread_watchdog thread_watchdog{buffer_, [this]() { watchdog_mock_.kick(); }};

    InSequence s;
    EXPECT_CALL(this_thread_, get_native_handle())
        .WillOnce(Return(satos::thread_native_handle(0xdeadc0de)));
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{10_s}));
    thread_watchdog.register_thread(1_s);

    EXPECT_THAT(buffer_[0].thread, Eq(satos::thread_native_handle(0xdeadc0de)));
    EXPECT_THAT(buffer_[0].last_feed_time, Eq(satos::clock::time_point{10_s}));
    EXPECT_THAT(buffer_[0].timeout, Eq(1_s));

    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{10500_ms}));
    EXPECT_CALL(watchdog_mock_, kick());
    thread_watchdog.kick();
}

TEST_F(ThreadWatchdogTest, KickTimeout) {
    watchdog::thread_watchdog thread_watchdog{buffer_, [this]() { watchdog_mock_.kick(); }};

    InSequence s;
    EXPECT_CALL(this_thread_, get_native_handle())
        .WillOnce(Return(satos::thread_native_handle(0xdeadc0de)));
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{10_s}));
    thread_watchdog.register_thread(1_s);

    EXPECT_THAT(buffer_[0].thread, Eq(satos::thread_native_handle(0xdeadc0de)));
    EXPECT_THAT(buffer_[0].last_feed_time, Eq(satos::clock::time_point{10_s}));
    EXPECT_THAT(buffer_[0].timeout, Eq(1_s));

    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{12_s}));
    thread_watchdog.kick();
}

} // namespace
