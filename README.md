[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sat-polsl_watchdog&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=sat-polsl_watchdog)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=sat-polsl_watchdog&metric=coverage)](https://sonarcloud.io/summary/new_code?id=sat-polsl_watchdog)
[![Pipeline](https://gitlab.com/sat-polsl/software/packages/watchdog/badges/main/pipeline.svg)](https://gitlab.com/sat-polsl/software/packages/watchdog/-/pipelines?page=1&scope=all&ref=main)
[![Version](https://img.shields.io/badge/dynamic/json?color=blue&label=Version&query=%24%5B%3A1%5D.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F45669677%2Frepository%2Ftags)](https://gitlab.com/sat-polsl/software/packages/watchdog/-/tags)

# Watchdog

## Overview

This library provides classes to monitor the state of multiple threads in the system,
detect deadlocked threads and provide a way to kick user-provided watchdogs if system is working properly.

### Rules

1. Use conventional commits.
2. Update tag with version after merging to `main`.
3. Use `snake_case` for everything except template arguments which are named with `CamelCase`

## Usage

Define watchdogs and register threads to monitor:

```cpp

hw_watchdog wdg;
thread monitored_thread;

watchdog::watchdog_buffer<1> watchdog_buffer; // (1)
watchdog::thread_watchdog watchdog(watchdog_buffer, [&wdg]() { wdg.kick(); }); // (2)

watchdog.register_thread(10_s, monitored_thread.native_handle()) // (3)

```

1. Create a buffer for watchdogs. The size of the buffer is the number of watchdogs you want to use.
2. Create a watchdog. The first argument is the buffer, the second is a callback that will be called when the watchdog
   is kicked.
3. Register a thread to monitor. The first argument is the timeout after which the thread will be considered not
   working, the second is the thread handle.

Thread shall periodically feed the watchdog:

```cpp
void thread::thread_function() {
    for (;;) {
        // do some work
        watchdog.feed();
    }
}
```

`watchdog.kick()` shall be called periodically in a thread or timer to check if the system is working properly and kick
the watchdog if it is.
